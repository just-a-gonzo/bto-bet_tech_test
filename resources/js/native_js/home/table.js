function favourtie_countries_fill_table(table_id, countries) {
    countries.forEach((country) => {
        add_row_to_table(table_id, country)
    });
}

function countries_fill_table(table_id, countries) {
    countries.forEach((country) => {
        $(table_id).append(`
            <div id="${country.name.official}_header" class="card-header">
                <h5 class="text-center">Country details</h5>
            </div>
            <div id="${country.name.official}_card" class="card">
                <b id="${country.name.official}_name">Name: ${country.name.official}</b>
                <b id="${country.name.official}_region">Region: ${country.region}</b>
                <b id="${country.name.official}_population">Population: ${country.population}</b>
                <button id="${country.name.official}_add_to_favourites" name="${country.name.official}" region="${country.region}" population="${country.population}" class="btn btn-light add_to_favourite"><b class="text-primary">Add to favorite</b></button>
            </div>
        `)
    });
}

function clear_table(table_id) {
    $(table_id).html("");
}

function add_row_to_table(table_id, country) {
    let comments = ``;

    if( typeof country.comments == 'undefined' ) country.comments = [];

    country.comments.forEach((comment) => {
        comments += get_html_comment_data(comment);
    })
    $(table_id).append(`
            <div id="favourite_${country.name}_header" class="card-header mt-5">
                <h5 class="text-center">Country details</h5>
            </div>
            <div id="favourite_${country.name}_card" class="card">
                <b id="favourite_${country.name}_name">Name: ${country.name}</b>
                <b id="favourite_${country.name}_region">Region: ${country.region}</b>
                <b id="favourite_${country.name}_population">Population: ${country.population}</b>
                <button country_name="${country.name}" country_id="${country.id}" id="favourite_${country.name}_add_to_favourites" class="btn btn-light remove_from_favourites"><b class="text-danger">Remove from favorite</b></button>
                <div class="d-flex ">
                    <input country_id="${country.id}" class="form-control w-75 " type="text" placeholder="Add a description">
                    <button country_name="${country.name}" country_id="${country.id}" class="btn btn-light w-25 add_comment"><span class="text-primary">Add</span></button>
                </div>
                ${comments}

            </div>
        `)
}

function get_html_comment_data(comment){
    return `
        <div class="card">
            <b>description: ${comment.comment}</b>
            <em>created_at: ${moment(comment.created_at).format('MMMM Do YYYY, h:mm:ss a')}</em>
        </div>
    `;
}

function add_comment_to_row(row_id, commentValue){
    let comment = {
        comment : commentValue,
        created_at: new Date()
    }
    $(`div[id="${row_id}"]`).append(get_html_comment_data(comment));
}
