@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">

            <button id="fetch_countries" type="button" class="btn btn-primary btn-lg btn-block mt-3 mb-5">
                Fetch countries
            </button>
            {{--                favouirte countries --}}
            <div class="card-header bg-secondary text-white">
                <h2 class="text-center">Favourite Countries</h2>
            </div>
            <br>
            <div id="favourite_countries" class="col-md-12 mt-4">
                {{--      here goes favourite countries  cards           --}}
            </div>

            <div class="m-5"></div>

            {{--                all countries --}}
            <div class="card-header bg-secondary text-white">
                <h2 class="text-center">Countries</h2>
            </div>
            <br>
            <div id="countries" class="col-md-12 mt-4">
                {{--      here goes  countries  cards           --}}
            </div>

        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/home/home.js') }}" type="text/javascript"></script>
@endsection
