<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\Country\CountryStoreRequest;
use App\Models\FavouriteCountry;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Auth;

class FavouriteCountryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return array
     */
    public function index()
    {
        // NOTE: the url from the task `https://restcountries.eu/` its not working so using different API
        $client = new Client();
        $restCountriesResponse = $client->request('GET', 'https://restcountries.com/v3.1/all');

        $response['countries'] = json_decode($restCountriesResponse->getBody()->getContents());
        $response['favourite_countries'] = FavouriteCountry::where('user_id','=',auth()->id())->with('comments')->get()->toArray();

        return $response;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryStoreRequest $request)
    {
        return FavouriteCountry::firstOrCreate([
            'user_id' => Auth::id(),
            'name' => $request->name,
        ],[
            'region' => $request->region,
            'population' => $request->population
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return FavouriteCountry::destroy($id);
    }
}
