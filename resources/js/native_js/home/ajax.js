async function fetch_all_countries() {
    let result = undefined;

    await axios.get('api/countries')
        .then(function (response) {
            result = response.data;
        })
        .catch(function (error) {
            $('#fetch_countries').attr('disabled', false);
            console.error(error);
        });

    return result;
}

async function store_country_as_favourite(name, region, population) {
    var result = {};

    await axios.post('api/countries', {
        name: name,
        region: region,
        population: population
    })
        .then( function (response)  {
            result = response.data
        })
        .catch((_response) => {
            console.error(_response)
        });

    return result;
}

function remove_country_from_favourites(country_id) {
    axios.delete('api/countries/'+country_id)
        .catch((_response) => {
            console.error(_response)
        });
}

function store_comment(favourite_country_id, comment){
    axios.post('api/comments', {
        favourite_country_id: favourite_country_id,
        comment: comment,
    })
        .catch((_response) => {
            console.error(_response)
        });
}
