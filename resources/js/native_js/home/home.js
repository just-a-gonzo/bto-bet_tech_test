$('#fetch_countries').click(async function (e) {
    $(this).attr('disabled', true)
    let result = await fetch_all_countries();
    $(this).attr('disabled', false)

    clear_table('#countries');
    countries_fill_table('#countries', result['countries'])

    clear_table('#favourite_countries');
    favourtie_countries_fill_table('#favourite_countries', result['favourite_countries'])
});



$('body').on('click', '.add_to_favourite', async function (e) {
    let countryData = await store_country_as_favourite($(this).attr('name'), $(this).attr('region'), $(this).attr('population'))

    let rowHeaderId = 'favourite_'+$(this).attr('name')+'_header';
    if( $(`div[id="${rowHeaderId}"]`).length < 1 ){
        add_row_to_table('#favourite_countries', countryData);
    }
});



$('body').on('click', '.remove_from_favourites', function (e) {
    remove_country_from_favourites($(this).attr('country_id'));

    let rowHeaderId = 'favourite_'+$(this).attr('country_name')+'_header';
    let rowBodyId = 'favourite_'+$(this).attr('country_name')+'_card';
    $(`div[id="${rowHeaderId}"]`).remove();
    $(`div[id="${rowBodyId}"]`).remove();
});



$('body').on('click', '.add_comment', function (e) {
    let country_id = $(this).attr('country_id');
    let comment = $(`input[country_id="${country_id}"]`).val();
    store_comment(country_id, comment);

    let rowBodyId = 'favourite_'+$(this).attr('country_name')+'_card';
    add_comment_to_row(rowBodyId, comment)

    $(`input[country_id="${country_id}"]`).val('');
});
