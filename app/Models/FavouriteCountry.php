<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FavouriteCountry extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'region',
        'population',
        'created_at',
        'updated_at'
    ];

    public function comments(){
        return $this->hasMany(Comment::class);
    }
}
